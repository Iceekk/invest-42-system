import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
// import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { MatTableDataSource, } from '@angular/material/table';
import {   MatSort } from '@angular/material/sort';
import {  MatPaginator } from '@angular/material/paginator';
import {   MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-client-history',
  templateUrl: './client-history.component.html',
  styleUrls: ['./client-history.component.css']
})
export class ClientHistoryComponent implements OnInit, AfterViewInit {

  displayedColumns: string[] = ['name', 'industry', 'units', 'buyprice', 'sellprice', 'dateopened'];
  dataSource = new MatTableDataSource<any>();
  index: number;
  id: number;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    private managerService: ManagerService,
    private dialog: MatDialog,
    private router: Router,
  ) { }


  ngOnInit() {
    this.getClientClosedOrders();
  }

  public getClientClosedOrders = () => {
    this.managerService.getClosedOrdersInfo(this.router.url.split('/')[3])
      .subscribe((res) => {
        this.dataSource.data = res;

      });
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private refreshTable() {
    this.getClientClosedOrders();
  }


}
