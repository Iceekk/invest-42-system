import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-manage-client',
  templateUrl: './manage-client.component.html',
  styleUrls: ['./manage-client.component.css']
})
export class ManageClientComponent implements OnInit {

  public managing = true;
  public navMode = 'Режим на менажиране';
  public navMenu = [
    {
      text: 'Портфолио',
      route: './portfolio',
      path: 'portfolio',
      isActive: false,
    },
    {
      text: 'Активни позиции',
      route: './positions',
      path: 'positions',
      isActive: false,
    },
    {
      text: 'Пазар',
      route: './market',
      path: 'market',
      isActive: false,
    },
    {
      text: 'Наблюдавани',
      route: './watchlist',
      path: 'watchlist',
      isActive: false,
    },
    {
      text: 'История',
      route: './history',
      path: 'history',
      isActive: false,
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
