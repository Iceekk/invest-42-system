import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/shared/core/authentication/authentication.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  public managing = false;
  public navMode = 'Режим на преглед';
  public navMenu = [
    {
      text: 'Пазар',
      route: '/manager/market',
      path: 'market',
      isActive: false,
    },
    {
      text: 'Позиции',
      route: '/manager/positions',
      path: 'positions',
      isActive: false,
    },
    {
      text: 'Клиенти',
      route: '/manager/clients',
      path: 'clients',
      isActive: false,
    }
  ];

  constructor(
    private authService: AuthenticationService,
  ) { }

  ngOnInit() {

  }

}
